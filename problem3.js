function problem3(inventory){
    if(inventory.length === 0){
        console.log(`Empty! There are no details in inventory array`)
    }
    else {
        let modelNames = [];
        for(let i=0;i<inventory.length;i++){
        modelNames.push(inventory[i]['car_model']);
        modelNames.sort();
        }
        console.log(modelNames);
    }
}

module.exports = problem3;