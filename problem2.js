function problem2(inventory){
    if(inventory.length === 0){
        console.log('Empty! There are no car details in inventory array.');
    }
    else{
        let carDetails;
        for(let i=0;i<inventory.length;i++){
            if(inventory[i]['id'] === inventory.length){
                carDetails =(inventory[i]);
            }
        }
        console.log(`Last car is a ${carDetails['car_make']} ${carDetails['car_model']}`)
    }
}

module.exports = problem2;
